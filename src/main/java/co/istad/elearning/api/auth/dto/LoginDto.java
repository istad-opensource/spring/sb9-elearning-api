package co.istad.elearning.api.auth.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;

@Builder
public record LoginDto(
        @NotBlank
        String email,
        @NotBlank
        String password
) {
}
