package co.istad.elearning.api.user;

import co.istad.elearning.api.user.dto.UserCreationDto;

public interface UserService {

    void createNew(UserCreationDto userCreationDto);

}
